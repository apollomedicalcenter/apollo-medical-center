Apollo Medical Center helps people with chronic and terminal illness improve their quality of life. We focus on providing the most affordable, high-quality medicinal cannabis solutions available in Vancouver.

Address: 1712 W 4th Ave, Vancouver, BC V6J 1M1, Canada

Phone: 604-559-9555
